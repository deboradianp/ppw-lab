from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Hello!\nThis is the landing page of a computer science student who loves playing her violin and sunny days.\nI have a serious love and hate relationship with programming, but I believe that difficult roads often lead to beautiful destinations.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)

